module Futrina.Launcher.Core.Settings where

import Control.Lens
import Data.Default
import Data.Text (Text)
import Futrina.Aeson
import Futrina.Launcher.Core.Lens

data Settings = Settings {
  settingsAuthServer :: !Text
} deriving (Eq, Show)

instance Default Settings where
  def = Settings {
      settingsAuthServer = "http://127.0.0.1:20122"-- "https://furtovina.com"
    }

$(deriveJSON (aesonOptionsStripPrefix "settings") ''Settings)

makeLensesWith humbleFields ''Settings
