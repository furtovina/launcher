module Futrina.Launcher.Core.Storage(
    storageDirName
  , getStorageDir
  , listInstalledVersions
  , isVersionInstalled
  , getVersionDirName
  ) where

import Control.Monad.IO.Class
import Data.Maybe
import Data.Text (pack, unpack)
import Futrina.API.Version
import System.Directory
import System.Directory.Tree
import System.FilePath

-- | Constant name of folder with versions
storageDirName :: FilePath
storageDirName = ".aerospace"

-- | Get full path to folder with all versions
getStorageDir :: MonadIO m => m FilePath
getStorageDir = do
  homeDir <- liftIO getHomeDirectory
  pure $ homeDir </> storageDirName

-- | Get list of installed versions of clients
listInstalledVersions :: MonadIO m => m [Version]
listInstalledVersions = do
  dir <- getStorageDir
  _ :/ tree <- liftIO $ build dir
  pure $ reverse $ case tree of
    Dir _ as -> catMaybes $ fmap getDirVer as
    _ -> []
  where
    getDirVer tree = case tree of
      Dir n _ -> parseVersion $ pack n
      _ -> Nothing

isVersionInstalled :: MonadIO m => Version -> m Bool
isVersionInstalled v = do
  vs <- listInstalledVersions
  pure $ v `elem` vs

getVersionDirName :: MonadIO m => Version -> m FilePath
getVersionDirName v = do
  sp <- getStorageDir
  pure $ sp </> unpack (showVersion v)
