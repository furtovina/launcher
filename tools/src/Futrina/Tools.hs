module Futrina.Tools(
    ServerUrl
  , futrinaSignin
  , futrinaVerify
  , futrinaGetNews
  , futrinaGetVersion
  , futrinaDownload
  , futrinaAddNews
  , futrinaAddVersion
  ) where

import Control.Monad.Except
import Data.Bifunctor
import Data.Text (Text, pack, unpack)
import Futrina.API.V1
import Futrina.Client
import Futrina.Crypto
import Network.Curl.Download.Lazy

import qualified Data.ByteString.Lazy as BSL

type ServerUrl = Text

callServer :: MonadIO m => ServerUrl -> ClientM a -> m (Either Text a)
callServer url ma = do
  mng <- createManager
  first (pack . show) <$> callFutrina url mng ma

futrinaSignin :: MonadIO m => ServerUrl -> Login -> PassHash -> m (Either Text Token)
futrinaSignin url login pass = fmap (second signupRespToken) $
  callServer url $ futrinaApiSignin api $ SigninReq login pass

futrinaVerify :: MonadIO m => ServerUrl -> Login -> Token -> m (Either Text Bool)
futrinaVerify url login token = callServer url $ futrinaApiVerify api $ VerifyReq login token

futrinaGetNews :: MonadIO m => ServerUrl -> PublicKey -> m (Either Text [NewsItem])
futrinaGetNews url pk = runExceptT $ do
  rawNews <- ExceptT $ callServer url $ futrinaApiNews api
  let checkNews (news, s)
        | verifyJsonSign pk news s = pure news
        | otherwise = throwError $ "Invalid signature for news record '" <> newsItemTitle news <> "'"
  traverse checkNews rawNews

futrinaGetVersion :: MonadIO m => ServerUrl -> PublicKey -> m (Either Text VersionInfo)
futrinaGetVersion url pk = runExceptT $ do
  (rawVinfo, s) <- ExceptT $ callServer url $ futrinaApiVersion api
  unless (verifyJsonSign pk rawVinfo s) $ throwError $ "Invalid signature of version metainfo: " <> showVersion (vinfoVersion rawVinfo)
  pure rawVinfo

-- | Download remote archive and check signature
futrinaDownload :: MonadIO m
  => Text -- ^ Url of remote archive
  -> Text -- ^ Expected file SHA256 hash
  -> FilePath -- ^ Path where to write down content of archive
  -> m (Either Text ())
futrinaDownload url expected path = runExceptT $ do
  bs <- ExceptT $ fmap (first pack) $ liftIO $ openLazyURI $ unpack url
  liftIO $ BSL.writeFile path bs
  unless (hashByteString bs == expected) $ throwError "Checksum of file is wrong!"

-- | Upload news record to remote server with signature
futrinaAddNews :: MonadIO m
  => Text -- ^ Url of remote server
  -> NewsItem -- ^ News record
  -> SecretKey -- ^ Private key to sign
  -> m (Either Text ())
futrinaAddNews url news key = do
  callServer url $ futrinaApiAddNews api (news, signJson key news)

-- | Upload new version to remote server with signature
futrinaAddVersion :: MonadIO m
  => Text -- ^ Url of remote server
  -> VersionInfo -- ^ Version info with url of already uploaded version
  -> SecretKey -- ^ Private key to sign
  -> m (Either Text ())
futrinaAddVersion url v key = do
  callServer url $ futrinaApiAddVersion api (v, signJson key v)
