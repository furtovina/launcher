module Futrina.Server.Config(
    Config(..)
  , loadConfig
  ) where

import Control.Monad.IO.Class
import Data.Text (Text)
import Data.Yaml.Config
import Futrina.Aeson
import GHC.Generics

data Config = Config {
  -- | Serving IP/interface of server
    configHost     :: !Text
  -- | Serving port of server
  , configPort     :: !Int
  -- | Location of database file
  , configDb       :: !FilePath
  -- | Location of folder that is statically served
  , configStatic   :: !FilePath 
  } deriving (Show, Generic)
deriveJSON (aesonOptionsStripPrefix "config") ''Config

-- | Load config from file
loadConfig :: MonadIO m => FilePath -> m Config
loadConfig path = liftIO $ loadYamlSettings [path] [] useEnv
