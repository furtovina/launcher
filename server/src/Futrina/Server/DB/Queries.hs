module Futrina.Server.DB.Queries(
    getUserPassHash
  , registerUser
  , queryUserToken
  , searchToken
  , getNews
  , getLastVersion
  , insertNews
  , insertVersion
  ) where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger
import Data.Either
import Data.Foldable (traverse_)
import Data.Maybe
import Data.Text (Text)
import Data.Time
import Database.Esqueleto
import Futrina.Aeson
import Futrina.API.News
import Futrina.API.Version
import Futrina.API.V1 (Login, PassHash, RawSign)
import Futrina.Server.DB.Monad
import Futrina.Server.DB.Schema
import Futrina.Server.Password
import Safe (headMay)

import qualified Data.UUID as UUID
import qualified Data.UUID.V4 as UUID

getUser :: MonadIO m => Login -> QueryT m (Maybe (Entity SignupRec))
getUser login = fmap headMay $ select $ from $ \ur -> do
  where_ (ur ^. SignupRecLogin ==. val login)
  pure ur

getUserPassHash :: MonadIO m => Login -> QueryT m (Maybe StoredPassHash)
getUserPassHash login = fmap (signupRecPassHash . entityVal) <$> getUser login

registerUser :: MonadIO m => Login -> PassHash -> QueryT m ()
registerUser login phash = do
  h <- liftIO $ hashPassword phash
  t <- liftIO $ getCurrentTime
  void $ insert $ SignupRec login h t Nothing Nothing

updateToken :: MonadIO m => Login -> Token -> UTCTime -> QueryT m ()
updateToken l token t = update $ \r -> do
  set r [ SignupRecToken =. val (Just token)
        , SignupRecTokenExpire =. val (Just t) ]
  where_ (r ^. SignupRecLogin ==. val l)

genToken :: MonadIO m => m Text
genToken = fmap UUID.toText $ liftIO $ UUID.nextRandom

queryUserToken :: MonadIO m => Login -> QueryT m Text
queryUserToken login = do
  t <- liftIO getCurrentTime
  mrec <- getUser login
  let genNew = do
        token <- genToken
        updateToken login token t
        pure token
  case mrec of
    Just (Entity _ SignupRec{..}) |
      Just e <- signupRecTokenExpire, e >= t -> maybe genNew pure signupRecToken
    _ -> genNew

-- | Search token and return its expire time
searchToken :: MonadIO m => Login -> Token -> QueryT m (Maybe UTCTime)
searchToken login tok = fmap (join . fmap unValue . headMay) $ select $ from $ \ur -> do
  where_ (ur ^. SignupRecToken ==. val (Just tok)
      &&. ur ^. SignupRecLogin ==. val login )
  pure $ ur ^. SignupRecTokenExpire

-- | Load all news from DB
getNews :: (MonadIO m, MonadLogger m) => QueryT m [(NewsItem, RawSign)]
getNews = do
  rs :: [Entity NewsRec] <- select $ from $ \r -> do
    orderBy [desc $ r ^. NewsRecCreated]
    pure r
  let ers = flip fmap rs $ \(Entity _ (NewsRec t m s ct)) -> do
        m' <- decodeJson m
        pure (NewsItem ct t m', s)
  flip traverse_ (filter isLeft ers) $ \case
    Left er -> logErrorN $ "getNews decoding error: " <> er
    _ -> pure ()
  pure $ catMaybes . fmap (either (const Nothing) Just) $ ers

getLastVersion :: MonadIO m => QueryT m (Maybe (VersionInfo, RawSign))
getLastVersion = do
  mv <- fmap (fmap entityVal . headMay) $ select $ from $ \r -> do
    orderBy [ desc $ r ^. VersionRecMajor
            , desc $ r ^. VersionRecMinor
            , desc $ r ^. VersionRecPatch ]
    limit 1
    pure r
  pure $ flip fmap mv $ \(VersionRec major minor patch meta url hash sign) ->
    (VersionInfo (Version major minor patch meta) url hash, sign)

insertNews :: MonadIO m => NewsItem -> RawSign -> QueryT m ()
insertNews (NewsItem dt t b) s = void . insert $ NewsRec t (encodeJson b) s dt

insertVersion :: MonadIO m => VersionInfo -> RawSign -> QueryT m ()
insertVersion (VersionInfo (Version major minor patch meta) url hash) sign =
  void . insert $ VersionRec major minor patch meta url hash sign
