module Futrina.Server.DB.Monad(
    MonadDB(..)
  , DBPool
  , QueryT
  , newDBPool
  , runDb
  , runMigration
  ) where

import Control.Concurrent (threadDelay)
import Control.Monad.IO.Unlift
import Control.Monad.Logger
import Control.Monad.Reader
import Data.Pool (Pool)
import Data.Text (pack)
import Database.Persist.Sql
import Database.Persist.Sqlite

import qualified Control.Exception as E
import qualified Database.Sqlite as Sqlite

type DBPool = Pool SqlBackend

newDBPool :: (MonadLogger m, MonadUnliftIO m) => FilePath -> m DBPool
newDBPool dbFile = do
  let info = mkSqliteConnectionInfo $ pack dbFile
  createSqlitePoolFromInfo info 10

class (MonadLogger m, MonadUnliftIO m) => MonadDB m where
  getDbPool :: m DBPool

type QueryT m a = ReaderT SqlBackend m a

instance (MonadLogger m, MonadUnliftIO m) => MonadDB (ReaderT DBPool m) where
  getDbPool = ask
  {-# INLINE getDbPool #-}

-- | Execute database action
runDb :: MonadDB m => ReaderT SqlBackend m a -> m a
runDb ma = do
  pool <- getDbPool
  flip runSqlPool pool $ retryOnBusy ma

-- | Retry if a Busy is thrown, following an exponential backoff strategy.
--
-- @since 2.9.3
--
-- Copied from persistent-sqlite as bex doesn't allow to use recent persistent.
retryOnBusy :: (MonadUnliftIO m, MonadLogger m) => m a -> m a
retryOnBusy action =
  start $ take 20 $ delays 1000
  where
    delays x
      | x >= 1000000 = repeat x
      | otherwise = x : delays (x * 2)

    start [] = do
      $logWarn "Out of retry attempts"
      action
    start (x:xs) = do
      -- Using try instead of catch to avoid creating a stack overflow
      eres <- withRunInIO $ \run -> E.try $ run action
      case eres of
        Left (Sqlite.SqliteException { Sqlite.seError = Sqlite.ErrorBusy }) -> do
          $logWarn "Encountered an SQLITE_BUSY, going to retry..."
          liftIO $ threadDelay x
          start xs
        Left e -> liftIO $ E.throwIO e
        Right y -> return y
