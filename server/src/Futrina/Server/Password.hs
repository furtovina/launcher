module Futrina.Server.Password(
    passwordMatch
  , hashPassword
  ) where

import Data.Text.Encoding (encodeUtf8)
import Futrina.API.V1 (PassHash)
import Futrina.Crypto (hashPassword)
import Futrina.Server.DB.Schema

import qualified Crypto.KDF.BCrypt as C

passwordMatch :: StoredPassHash -> PassHash -> Bool
passwordMatch a b = C.validatePassword (encodeUtf8 b) (encodeUtf8 a)
