module Futrina.Server.App(
    futrinaAuthApp
  ) where

import Data.Proxy
import Futrina.API
import Futrina.Server.Config
import Futrina.Server.Monad
import Futrina.Server.Server
import Network.Wai
import Network.Wai.Middleware.Cors
import Network.Wai.Middleware.Gzip
import Servant.API
import Servant.API.Generic
import Servant.Server
import Servant.Server.StaticFiles

type StaticApi = "static" :> Raw
type FullAPI = ToServantApi FutrinaVerApi :<|> StaticApi

fullApi :: Proxy FullAPI
fullApi = Proxy

futrinaAuthApp :: Env -> Application
futrinaAuthApp e = gzip def . appCors $ serve fullApi $ apiServer :<|> staticServer
  where
    Config{..} = envConfig e
    appCors = cors $ const $ Just simpleCorsResourcePolicy
      { corsRequestHeaders = ["Content-Type"]
      , corsMethods = "PUT" : simpleMethods }
    apiServer = hoistServer futrinaApi (runServerM e) $ toServant futrinaAuthServer
    staticServer = serveDirectoryWebApp configStatic
