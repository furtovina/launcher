module Futrina.Server.Monad(
    Env(..)
  , newEnv
  , AsServerM
  , ServerM
  , runServerM
  , runServerMIO
  , runDb
  , getConfig
  , notFound
  , badRequest
  , internalError
  , guardAuthed
  , guardSign
  , liftIO
  , logDebugN
  , logInfoN
  , logWarnN
  , logErrorN
  , module X
  ) where

import Control.Concurrent
import Control.Monad.Catch hiding (Handler)
import Control.Monad.Except
import Control.Monad.IO.Unlift
import Control.Monad.Logger
import Control.Monad.Reader
import Data.Aeson (ToJSON)
import Data.Text (Text)
import Data.Text.Encoding
import Data.Time as X (UTCTime, getCurrentTime)
import Futrina.API.Key (RawSign, aerospacePublicKey)
import Futrina.Crypto
import Futrina.Server.Config
import Futrina.Server.DB
import Servant.Server
import Servant.Server.Generic

import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T

data Env = Env {
  envConfig :: !Config
, envLogger :: !(Chan (Loc, LogSource, LogLevel, LogStr))
, envPool   :: !DBPool
, envPubKey :: !PublicKey
}

newEnv :: MonadIO m => Config -> m Env
newEnv cfg = do
  logger <- liftIO newChan
  void . liftIO . forkIO $ runStdoutLoggingT $ unChanLoggingT logger
  pool <- liftIO . runStdoutLoggingT $ do
    pool <- newDBPool $ configDb cfg
    flip runReaderT pool $ runDb $ runMigration migrateAll
    pure pool
  pk <- either (fail . T.unpack) pure $ parsePubKey aerospacePublicKey
  pure Env {
      envConfig = cfg
    , envLogger = logger
    , envPool   = pool
    , envPubKey = pk
    }

-- | For transforming records with endpoints into records of handlers
type AsServerM = AsServerT ServerM

-- | Context for endpoints of server
newtype ServerM a = ServerM { unServerM :: ReaderT Env (LoggingT IO) a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadLogger, MonadReader Env, MonadThrow, MonadCatch, MonadMask)

-- | Catch thrown servant exceptions
catchHandler :: IO a -> Handler a
catchHandler = Handler . ExceptT . try

-- | Run server monad in servant handler (don't print logs from channel where they are accumulated)
runServerM :: Env -> ServerM a -> Handler a
runServerM e = catchHandler . runChanLoggingT (envLogger e) . flip runReaderT e . unServerM

-- | Execution of 'ServerM' in IO monad
runServerMIO :: Env -> ServerM a -> IO a
runServerMIO env m = do
  ea <- runHandler $ runServerM env m
  case ea of
    Left e -> fail $ "runServerMIO: " <> show e
    Right a -> return a

-- | Get current server config
getConfig :: ServerM Config
getConfig = asks envConfig

-- | Throw 404 for Nothing
notFound :: Maybe a -> ServerM a
notFound = maybe (throwM err404) pure

-- | Throw 400 for Left
badRequest :: Either Text a -> ServerM a
badRequest = either (\s -> throwM err400 { errBody = BSL.fromStrict $ encodeUtf8 s }) pure

-- | Throw 500 for Left
internalError :: Either Text a -> ServerM a
internalError = either (\s -> throwM err500 { errBody = BSL.fromStrict $ encodeUtf8 s }) pure

-- | Throw 401 for Nothing
guardAuthed :: Maybe a -> ServerM a
guardAuthed = maybe (throwM err401) pure

-- | Check that value is signed with aerospace key
guardSign :: ToJSON a => a -> RawSign -> ServerM ()
guardSign a s = do
  pk <- asks envPubKey
  unless (verifyJsonSign pk a s) $ throwM err401

instance MonadDB ServerM where
  getDbPool = asks envPool
  {-# INLINE getDbPool #-}

instance MonadUnliftIO ServerM where
  askUnliftIO = ServerM (fmap (\(UnliftIO run) -> UnliftIO (run . unServerM)) askUnliftIO)
  withRunInIO go = ServerM (withRunInIO (\k -> go (k . unServerM)))
