{config, lib, pkgs, ...}:
with lib;
let
  # the values of the options set for the service by the user of the service
  cfg = config.services.futrina-auth-server;
  backupScript = pkgs.writeShellScriptBin "futrina-auth-backup" ''

  '';
in {
  ##### interface. here we define the options that users of our service can specify
  options = {
    services.futrina-auth-server = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Whether to enable futrina-auth-server server process by default.
        '';
      };
      config = mkOption {
        type = types.str;
        description = ''
          Configuration file for server.
        '';
        default = ''
        host: 127.0.0.1
        port: ${builtins.toString cfg.port}
        db: ${cfg.data}/auth.db
        static: ${cfg.static}
        '';
      };
      data = mkOption {
        type = types.str;
        default = "/var/futrina-auth-server/server";
        description = ''
          Path to working folder of server.
        '';
      };
      static = mkOption {
        type = types.str;
        default = "/var/futrina-auth-server/server/static";
        description = ''
          Path to file serving folder of server.
        '';
      };
      port = mkOption {
        type = types.int;
        default = 8080;
        description = ''
          Listen port of server.
        '';
      };
    };
  };

  ##### implementation
  config = mkIf cfg.enable { # only apply the following settings if enabled
    environment.etc."futrina-auth-server.conf" = {
      text = cfg.config;
    };
    # Service user
    users.users.futrina-auth-server = {
      createHome = false;
      description = "User for futrina auth server";
      extraGroups = [ ];
      isSystemUser = false;
      useDefaultShell = true;
    };
    users.groups.futrina-auth-server = {
      members = [ "futrina-auth-server" ];
    };

    # Create systemd service
    systemd.services.futrina-auth-server = {
      enable = true;
      description = "Auth server for futrina";
      after = ["network.target"];
      wants = ["network.target"];
      serviceConfig = {
          Restart = "always";
          RestartSec = 30;
          User = "futrina-auth-server";
          Group = "futrina-auth-server";
          ExecStart = ''
            ${pkgs.futrina-auth-server}/bin/futrina-auth-server listen /etc/futrina-auth-server.conf
          '';
        };
      wantedBy = ["multi-user.target"];
    };
    # Write shortcut script to run commands for all nodes
    environment.systemPackages = [
      backupScript
    ];
    # Enable cron service for backups
    services.cron = {
      enable = true;
      systemCronJobs = [
        "0 0,4,8,12,16,20 * * *      root    ${backupScript}/bin/futrina-auth-backup"
      ];
    };
    system.activationScripts = {
      create-futrina-auth-server-data = {
        text = ''
        if [ ! -d "${cfg.data}" ]; then
          echo "Creating folder for futrina-auth-server at ${cfg.data}..."
          mkdir -p "${cfg.data}"
          chown futrina-auth-server "${cfg.data}"
        fi
        if [ ! -d "${cfg.static}" ]; then
          echo "Creating static folder for futrina-auth-server at ${cfg.static}..."
          mkdir -p "${cfg.static}"
          chown futrina-auth-server "${cfg.static}"
        fi
        '';
        deps = [ ];
      };
    };
  };
}
