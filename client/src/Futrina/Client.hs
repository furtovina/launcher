module Futrina.Client(
    createManager
  , callFutrina
  , api
  , ClientM
  , Manager
  , ServantError
  , module Futrina.API
  , module Futrina.API.V1
  ) where

import Control.Monad.IO.Class
import Data.Proxy
import Data.Text (Text, pack, unpack)
import Futrina.API
import Futrina.API.V1
import Network.HTTP.Client hiding (Proxy)
import Network.HTTP.Client.TLS    (tlsManagerSettings)
import Servant.API.Generic
import Servant.Client

-- | Helper to allocate new TLS connection manager
createManager :: MonadIO m => m Manager
createManager = liftIO $ newManager tlsManagerSettings

-- | Perform remote call to futrina auth server
callFutrina :: MonadIO m
  => Text -- ^ Base url
  -> Manager -- ^ Connection manager
  -> ClientM a -- ^ Remote action
  -> m (Either ServantError a)
callFutrina ulr mng ma = case parseBaseUrl . unpack $ ulr of
  Left e -> pure $ Left . ConnectionError . pack . show $ e
  Right burl -> liftIO $ runClientM ma $ ClientEnv mng burl Nothing

data AsClient
instance GenericMode AsClient where
  type AsClient :- api = Client ClientM api

-- | Get structure with endpoints. Use fields from `FutrinaApi` and `callFutrina`
-- to execute them.
api :: FutrinaApi AsClient
api = fromServant $ futrinaApi'v1 vapi
  where
    vapi :: FutrinaVerApi AsClient
    vapi = fromServant $ client (Proxy :: Proxy (ToServant FutrinaVerApi AsApi))
