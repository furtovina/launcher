module Futrina.API.Version(
    Version(..)
  , VersionInfo(..)
  , showVersion
  , parseVersion
  ) where

import Data.Attoparsec.Text
import Data.Text (Text)
import Futrina.Aeson
import Futrina.Text
import GHC.Generics

-- | Semantic version
data Version = Version {
    versionMajor :: !Int -- ^ Breaking changes
  , versionMinor :: !Int -- ^ New features (backward compatible)
  , versionPatch :: !Int -- ^ Bugfixes
  , versionMeta  :: !(Maybe Text) -- ^ Additional metainfo
  } deriving (Generic, Show, Eq)
$(deriveJSON (aesonOptionsStripPrefix "version") ''Version)

-- | Show version as major.minor.patch-meta
showVersion :: Version -> Text
showVersion Version{..} = showt versionMajor
                <> "." <> showt versionMinor
                <> "." <> showt versionPatch
                <> (maybe "" ("-" <>) versionMeta)

-- | Parse version from string
parseVersion :: Text -> Maybe Version
parseVersion = either (const Nothing) Just . parseOnly (versionParser <* endOfInput)

-- | Attoparsec parser for version
versionParser :: Parser Version
versionParser = do
  versionMajor <- decimal
  _ <- char '.'
  versionMinor <- decimal
  _ <- char '.'
  versionPatch <- decimal
  versionMeta <- option Nothing $ fmap Just metaParser
  pure Version{..}
  where
    metaParser = do
      _ <- string "-"
      r <- takeTill isEndOfLine
      pure r

-- | Version with info where to get the file
data VersionInfo = VersionInfo {
  vinfoVersion :: !Version
, vinfoUrl     :: !Text -- ^ Direct url to download gziped archive
, vinfoHash    :: !Text -- ^ SHA256 hash of archive
} deriving (Generic, Show, Eq)
$(deriveJSON (aesonOptionsStripPrefix "vinfo") ''VersionInfo)
