{ }:
let
  reflex-platform = import ./reflex-platform.nix {};
in reflex-platform.project ({ pkgs, ... }: {
  packages = {
    futrina-api           = ./api;
    futrina-auth-server   = ./server;
    futrina-client        = ./client;
    futrina-crypto        = ./crypto;
    futrina-launcher      = ./launcher;
    futrina-launcher-gui  = ./launcher-gui;
    futrina-tools         = ./tools;
  };
  shells = {
    ghc = [
      "futrina-api"
      "futrina-auth-server"
      "futrina-client"
      "futrina-crypto"
      "futrina-launcher"
      "futrina-launcher-gui"
      "futrina-tools"
    ];
  };
  overrides = import ./overrides.nix { inherit reflex-platform; };
})
