module Futrina.Crypto(
    RawPrivKey
  , PublicKey
  , SecretKey
  , parsePrivKey
  , parsePubKey
  , parseSign
  , showPubKey
  , showPrivKey
  , showSign
  , verifyJsonSign
  , guardJsonSign
  , guardAerospaceSign
  , signJson
  , prehashPassword
  , hashPassword
  , hashByteString
  , generatePrivKey
  , toPublic
  ) where

import Crypto.Error (eitherCryptoError, CryptoFailable)
import Crypto.Hash
import Crypto.PubKey.Ed25519
import Crypto.Random.Types (MonadRandom)
import Data.Aeson
import Data.Bifunctor
import Data.ByteArray (convert, ByteArrayAccess)
import Data.ByteString (ByteString)
import Data.Text (Text, pack)
import Data.Text.Encoding (encodeUtf8, decodeUtf8With)
import Data.Text.Encoding.Error (lenientDecode)
import Futrina.Aeson
import Futrina.API.Key
import Futrina.API.V1 (PassHash)

import qualified Crypto.KDF.BCrypt as BC
import qualified Data.ByteString as BS
import qualified Data.ByteString.Base64 as B64
import qualified Data.ByteString.Lazy as BSL

-- | Base64 ed25519 private key
type RawPrivKey = Text

-- | Parse base64 encoded crypto values
parseCrypto :: (ByteString -> CryptoFailable a) -> Text -> Either Text a
parseCrypto f t = do
  bs <- first pack . B64.decode . encodeUtf8 $ t
  first (pack . show) . eitherCryptoError . f $ bs

parsePubKey :: RawPubKey -> Either Text PublicKey
parsePubKey = parseCrypto publicKey

parsePrivKey :: RawPrivKey -> Either Text SecretKey
parsePrivKey = parseCrypto secretKey

parseSign :: RawSign -> Either Text Signature
parseSign = parseCrypto signature

verifyJsonSign :: ToJSON a => PublicKey -> a -> RawSign -> Bool
verifyJsonSign pk a s = either (const False) id $ do
  s' <- parseSign s
  pure $ verify pk (encodeUtf8 . encodeJson $ a) s'

guardJsonSign :: ToJSON a => PublicKey -> (a, RawSign) -> Either Text a
guardJsonSign pk (a, s)
  | verifyJsonSign pk a s = Right a
  | otherwise = Left "Signature of response is not valid!"

guardAerospaceSign :: ToJSON a => (a, RawSign) -> Either Text a
guardAerospaceSign as = do
  k <- parsePubKey aerospacePublicKey
  guardJsonSign k as

signJson :: ToJSON a => SecretKey -> a -> RawSign
signJson k a = showSign s
  where
    bs = encodeUtf8 . encodeJson $ a
    s = sign k (toPublic k) bs

-- | Hash password to obusficate it from server
prehashPassword :: Text -> PassHash
prehashPassword = pack . show . (hash :: BS.ByteString -> Digest Blake2bp_512) . encodeUtf8

-- | Hash password (bcrypt) with sufficient amount of times
hashPassword :: forall m . MonadRandom m => Text -> m PassHash
hashPassword = fmap (decodeUtf8With lenientDecode) . BC.hashPassword 14 . encodeUtf8

-- | Hash lazy bytestring with SHA256
hashByteString :: BSL.ByteString -> Text
hashByteString = pack . show . (hashlazy :: BSL.ByteString -> Digest SHA256)

-- | Generate new private key
generatePrivKey :: MonadRandom m => m SecretKey
generatePrivKey = generateSecretKey

b64Show :: ByteArrayAccess a => a -> Text
b64Show = decodeUtf8With lenientDecode . B64.encode . convert

-- | Encode with base64 and convert to text
showPubKey :: PublicKey -> RawPubKey
showPubKey = b64Show

-- | Encode with base64 and convert to text
showPrivKey :: SecretKey -> RawPrivKey
showPrivKey = b64Show

-- | Encode with base64 and convert to text
showSign :: Signature -> RawSign
showSign = b64Show
