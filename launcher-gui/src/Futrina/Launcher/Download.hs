module Futrina.Launcher.Download(
    downloadVersion
  , DownloadResult(..)
  ) where

import Control.Concurrent
import Control.Monad.IO.Class
import Data.Functor (void)
import Data.Text (Text, unpack, pack)
import Futrina.API.V1
import Futrina.Launcher.Env
import Futrina.Launcher.Core.Storage
import Reflex.Dom
import System.Directory
import System.IO.Temp
import GHC.IO.Handle (hClose)

import Network.Download
import qualified Codec.Archive.Zip as Z
import qualified Data.ByteString as BS

data DownloadResult = Downloaded !VersionInfo | DownloadError

downloadVersion :: MonadLauncher t m => Event t VersionInfo -> m (Event t DownloadResult)
downloadVersion downloadE = mdo
  let statusE = leftmost [
         "Скачивание..." <$ downloadE
        , ("Oшибка скачивания: " <>) <$> downloadErrE
        ]
  widgetHold_ (pure ()) $ divClass "download-status" . text <$> statusE
  mdownE :: Event t (Either Text VersionInfo) <- performEventAsync $ ffor downloadE $ \vinfo@VersionInfo{..} fire -> void . liftIO . forkIO $ do
    putStrLn $ "Downloading " <> unpack vinfoUrl <> " ..."
    res <- openURI (unpack vinfoUrl)
    case res of
      Left e -> fire $ Left . pack $ e
      Right bs -> do
        putStrLn $ "Downloaded " <> show (BS.length bs) <> " bytes"
        withSystemTempFile (unpack $ "aerospace-" <> showVersion vinfoVersion) $ \archivePath archiveHandle -> do
          BS.hPut archiveHandle bs
          hClose archiveHandle
          path <- getVersionDirName vinfoVersion
          putStrLn $ "Extracting to " <> path <> " ..."
          createDirectoryIfMissing True path
          Z.withArchive archivePath $ Z.unpackInto path
          putStrLn "Extracted"
          fire $ Right vinfo
  let downloadErrE = fmapMaybe (either Just (const Nothing)) mdownE
      downloadedE = fmapMaybe (either (const Nothing) Just) mdownE
  pure $ leftmost [ fmap Downloaded downloadedE, DownloadError <$ downloadErrE ]
