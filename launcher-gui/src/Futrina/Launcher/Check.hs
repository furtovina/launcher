module Futrina.Launcher.Check(
    checkVersion
  , CheckResult(..)
  ) where

import Control.Concurrent
import Control.Monad.IO.Class
import Data.Functor (void)
import Futrina.API.V1
import Futrina.Client
import Futrina.Launcher.Env
import Futrina.Launcher.Core.Storage
import Futrina.Text
import Reflex.Dom

data CheckResult = UpToDate !VersionInfo | NeedUpdate !VersionInfo | UpdateMalformed

checkVersion :: MonadLauncher t m => Event t () -> m (Event t CheckResult)
checkVersion checkE = mdo
  let statusE = leftmost [
          "Получение версии..." <$ checkE
        , "Сверка версии..." <$ remoteE
        , ("Ошибка запроса версии: " <>) <$> verErrE
        ]
  widgetHold_ (pure ()) $ divClass "check-status" . text <$> statusE
  -- Getting last version
  remoteE <- callSClient $ ffor checkE $ const $ futrinaApiVersion api
  let verErrE = fforMaybe remoteE $ \case
        Left e -> Just $ showt e
        _ -> Nothing
      verE = fmapMaybe (either (const Nothing) Just) remoteE
  -- Checking installed version
  installedE <- performEventAsync $ ffor verE $ \vinfo@VersionInfo{..} fire -> void . liftIO . forkIO $ do
    installed <- isVersionInstalled vinfoVersion
    fire (vinfo, installed)
  let skipDownloadE = fforMaybe installedE $ \(vinfo, installed) ->
        if installed then Just vinfo else Nothing
      downloadE = fforMaybe installedE $ \(vinfo, installed) ->
        if installed then Nothing else Just vinfo

  pure $ leftmost [UpToDate <$> skipDownloadE, NeedUpdate <$> downloadE, UpdateMalformed <$ verErrE]
