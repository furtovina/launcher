module Futrina.Launcher.Login(
    loginForm
  ) where

import Data.Functor (void)
import Data.Text (Text)
import Futrina.API.V1
import Futrina.Client
import Futrina.Crypto
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Input
import Futrina.Launcher.Validate
import Futrina.Text
import Network.HTTP.Types.Status
import Reflex.Dom
import Servant.Client.Core

loginForm :: MonadLauncher t m => m (Event t Token)
loginForm = divClass "login-form" $ do
  resE <- form . fieldset $ do
    nameD <- textField "Name" ""
    passD <- passField "Password"
    btnE <- submitClass "button-primary" "Launch"
    let authE = tag (current $ (,) <$> nameD <*> passD) btnE
    signin authE
  spanClass "autoreg-info" $ par $ text "Регистрация происходит автоматически при первом входе на сервер"
  pure resE

type Password = Text

signin :: MonadLauncher t m => Event t (Login, Password) -> m (Event t Token)
signin e = do
  mres :: Event t (Either ServantError SigninResp) <- callClient $ ffor e $
    \(login, pass) -> futrinaApiSignin api (SigninReq login $ prehashPassword pass)
  validate (void e) $ ffor mres $ \case
    Left er -> Left $ case er of
      FailureResponse r | responseStatusCode r == status401 -> "Неверный пароль"
      _ -> showt er
    Right a -> Right $ signupRespToken a
