module Futrina.Launcher.Main(
    frontend
  ) where

import Control.Monad (void)
import Futrina.API.V1
import Futrina.Launcher.Check
import Futrina.Launcher.Download
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Launch
import Futrina.Launcher.Log
import Futrina.Launcher.Login
import Futrina.Launcher.News
import Reflex.Dom

frontend :: MonadLauncher t m => m ()
frontend = void $ retractStack mainPage

mainPage :: MonadLauncher t m => m ()
mainPage = container $ do
  h2 $ text "Aerospace"
  _ <- workflow checkVerWorkflow
  pure ()

checkVerWorkflow :: MonadLauncher t m => Workflow t m ()
checkVerWorkflow = Workflow $ do
  newsWidget
  rec tokenE <- switch . current <$> widgetHold loginForm (pure never <$ tokenE)
  checkE <- checkVersion (void tokenE)
  let nextE = ffor checkE $ \case
        UpToDate vinfo -> launchWorkflow vinfo
        NeedUpdate vinfo -> downloadWorkflow vinfo
        UpdateMalformed -> Workflow $ do
          returnE <- delay 5 =<< getPostBuild
          pure ((), checkVerWorkflow <$ returnE)
  pure ((), nextE)

launchWorkflow :: MonadLauncher t m => VersionInfo -> Workflow t m ()
launchWorkflow vinfo = Workflow $ mdo
  killE <- logWidget $ instanceOutputE inst
  inst <- launchVersion vinfo
  stopedE <- stopInstance inst killE
  let nextE = ffor stopedE $ const $ checkVerWorkflow
  pure ((), nextE)

downloadWorkflow :: MonadLauncher t m => VersionInfo -> Workflow t m ()
downloadWorkflow vinfo = Workflow $ do
  newsWidget
  buildE <- delay 0.1 =<< getPostBuild
  resE <- downloadVersion $ vinfo <$ buildE
  let nextE = ffor resE $ \case
        Downloaded _ -> launchWorkflow vinfo
        DownloadError -> Workflow $ do
          returnE <- delay 5 =<< getPostBuild
          pure ((), checkVerWorkflow <$ returnE)
  pure ((), nextE)
