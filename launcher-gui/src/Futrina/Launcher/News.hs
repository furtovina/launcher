{-# LANGUAGE OverloadedLists #-}
module Futrina.Launcher.News(
    newsWidget
  ) where

import Data.Foldable (traverse_)
import Data.Text (Text)
import Data.Time
import Futrina.API.News
import Futrina.Client
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Markup
import Reflex.Dom

import qualified Data.Text as T

newsWidget :: MonadLauncher t m => m ()
newsWidget = divClass "news" $ do
  buildE <- getPostBuild
  newsE <- futrinaGetNews buildE
  widgetHold_ (pure ()) $ ffor newsE $ \case
    Left er -> h2 $ spanClass "news-error" $ text . printClientError $ er
    Right news -> traverse_ renderNewsItem news

futrinaGetNews :: MonadLauncher t m => Event t () -> m (Event t (Either ClientError [NewsItem]))
futrinaGetNews e = callSClientM $ ffor e $ const $ futrinaApiNews api

newsDate :: UTCTime -> Text
newsDate = T.pack . formatTime defaultTimeLocale "%Y-%m-%d %H:%M"

renderNewsItem :: MonadLauncher t m => NewsItem -> m ()
renderNewsItem NewsItem{..} = divClass "news-block" $ do
  spanClass "news-title" $ h2 $ text newsItemTitle
  divClass "news-date" $ text $ newsDate newsItemDate
  renderMarkup newsItemBody
