{-# LANGUAGE CPP #-}
module Futrina.Launcher.Launch(
    launchVersion
  , LaunchInstance(..)
  , instanceOutputE
  , stopInstance
  ) where

import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.IO.Unlift
import Data.Foldable
import Data.Text (Text)
import Futrina.API.V1
import Futrina.Launcher.Env
import Futrina.Launcher.Core.Storage
import GHC.IO.Handle (Handle, hSetBuffering, BufferMode(..))
import Reflex.Dom
import System.Posix.Types
import System.Process.Typed

import qualified Control.Exception as E
import qualified Data.Text.IO as T
import qualified System.Process.Internals as P

data LaunchInstance t = LaunchInstance {
  instanceProcess :: Process () Handle Handle
, instanceStdout  :: Event t Text
, instanceStderr  :: Event t Text
}

instanceOutputE :: Reflex t => LaunchInstance t -> Event t Text
instanceOutputE LaunchInstance{..} = mergeWith (\a b -> a <> "\n" <> b) [instanceStdout, instanceStderr]

stopInstance :: MonadLauncher t m => LaunchInstance t -> Event t () -> m (Event t ())
stopInstance LaunchInstance{..} e = performFork $ ffor e $ const $ do
#if defined(WINDOWS)
#else
  liftIO $ do
    mpid <- getPid (unsafeProcessHandle instanceProcess)
    flip traverse_ mpid $ \(CPid pid) -> do
      withProcessWait_ (shell $ "kill -9 $(ps -o pid= --ppid " <> show pid <> ")") (const $ pure ())
#endif
  stopProcess instanceProcess

launchVersion :: MonadLauncher t m => VersionInfo -> m (LaunchInstance t)
launchVersion vinfo = mdo
  (stdoutE, stdoutFire) <- newTriggerEvent
  (stderrE, stderrFire) <- newTriggerEvent
  buildE <- delay 0.1 =<< getPostBuild
  path <- getVersionDirName $ vinfoVersion vinfo
  let procConfig :: ProcessConfig () Handle Handle
      procConfig =
          setStdout createPipe
        $ setStderr createPipe
        $ setWorkingDir path
        $ "./start_linux.sh"
  _ <- liftIO $ withProcessWait_ (shell $ "chmod a+x \"" <> path <> "/start_linux.sh\"") (const $ pure ())
  process <- liftIO $ startProcess procConfig
  let listenHandle h fire = performFork_ $ ffor buildE $ const $ liftIO $ do
        hSetBuffering h LineBuffering
        forever $ fire =<< T.hGetLine h
  listenHandle (getStdout process) stdoutFire
  listenHandle (getStderr process) stderrFire
  pure LaunchInstance {
      instanceProcess = process
    , instanceStdout = stdoutE
    , instanceStderr = stderrE
    }

-- | Same as 'withProcessWait', but also calls 'checkExitCode'
--
-- @since 0.2.5.0
withProcessWait_ :: (MonadUnliftIO m)
  => ProcessConfig stdin stdout stderr
  -> (Process stdin stdout stderr -> m a)
  -> m a
withProcessWait_ config f = bracket
    (startProcess config)
    stopProcess
    (\p -> f p <* checkExitCode p)

bracket :: MonadUnliftIO m => IO a -> (a -> IO b) -> (a -> m c) -> m c
bracket before after thing = withRunInIO $ \r -> E.bracket before after (r . thing)

-- | Returns the PID (process ID) of a subprocess.
--
-- 'Nothing' is returned if the handle was already closed. Otherwise a
-- PID is returned that remains valid as long as the handle is open.
-- The operating system may reuse the PID as soon as the last handle to
-- the process is closed.
--
-- @since 1.6.3.0
getPid :: P.ProcessHandle -> IO (Maybe CPid)
getPid (P.ProcessHandle mh _ _) = do
  p_ <- readMVar mh
  case p_ of
#ifdef WINDOWS
    P.OpenHandle h -> do
      pid <- getProcessId h
      return $ Just pid
#else
    P.OpenHandle pid -> return $ Just pid
#endif
    _ -> return Nothing
