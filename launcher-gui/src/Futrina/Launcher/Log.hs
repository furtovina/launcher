{-# LANGUAGE OverloadedLists #-}
module Futrina.Launcher.Log(
    logWidget
  ) where

import Control.Lens
import Control.Monad
import Data.Text (Text)
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Language.Javascript.JSaddle
import Reflex
import Reflex.Dom

import qualified Data.Text as T

scrollToBottom :: MonadJSM m => RawElement GhcjsDomSpace -> m ()
scrollToBottom elmnt = liftJSM $ do
  _ <- eval ("launcher_scrollToBottom = function(e) { e.scrollTop = e.scrollHeight; }" :: Text)
  void $ liftJSM $ jsg1 ("launcher_scrollToBottom" :: Text) (toJSVal elmnt)

-- | Max log size in chars
maxLogSize :: Int
maxLogSize = 10000

-- | Trim size of log if it too big
trimLog :: Text -> Text
trimLog a = if T.length a > maxLogSize then T.takeEnd maxLogSize a else a

logWidget :: forall t m . MonadLauncher t m => Event t Text -> m (Event t ())
logWidget logE = divClass "log-container" $ do
  logD <- foldDyn (\v acc -> trimLog $ acc <> "\n" <> v) "" logE
  tarea <- textAreaElement $ (def :: TextAreaElementConfig EventResult t (DomBuilderSpace m))
        & textAreaElementConfig_setValue .~ updated logD
        & textAreaElementConfig_elementConfig .~ (def
        & elementConfig_initialAttributes .~ [ ("readonly", "") ] )
  killE <- buttonClass "button-primary" "Kill"
  scrollD <- toggleClass "button-primary" "AutoScroll" True
  let elmnt = _element_raw $ _textAreaElement_element tarea
  dlogE <- delay 0.1 logE
  performEvent_ $ ffor dlogE $ const $ do
    doScroll <- sample . current $ scrollD
    when doScroll $ scrollToBottom elmnt
  pure killE
