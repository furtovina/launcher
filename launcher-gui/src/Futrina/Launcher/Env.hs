{-# LANGUAGE UndecidableInstances #-}
module Futrina.Launcher.Env(
    MonadLauncher(..)
  , MonadLauncherConstr
  , Env(..)
  , newEnv
  , runEnv
  , callClient
  , ClientError(..)
  , printClientError
  , callSClient
  , callSClientM
  , module Reflex.Dom.Retractable.Class
  , module Futrina.Launcher.Async
  ) where

import Control.Concurrent
import Control.Monad.Fix
import Control.Monad.Reader
import Control.Monad.IO.Unlift
import Data.Aeson.Types
import Data.Text (Text)
import Futrina.Client
import Futrina.Crypto
import Futrina.Launcher.Async
import Futrina.Launcher.Core.Settings
import Futrina.Text
import GHC.Generics
import Language.Javascript.JSaddle
import Reflex
import Reflex.Dom
import Reflex.Dom.Retractable.Class
import Reflex.Dom.Retractable.Trans

type MonadLauncherConstr t m = (MonadHold t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , PerformEvent t m
  , MonadIO (Performable m)
  , MonadUnliftIO (Performable m)
  , MonadSample t (Performable m)
  , MonadJSM (Performable m)
  , MonadIO m
  , TriggerEvent t m
  , DomBuilderSpace m ~ GhcjsDomSpace
  , MonadRetract t m)

class MonadLauncherConstr t m => MonadLauncher t m | m -> t where
  getSettings :: m Settings
  getManager :: m Manager

data Env = Env {
  env'settings  :: !Settings
, env'manager   :: !Manager
}

newEnv :: MonadIO m => Settings -> m Env
newEnv settings = liftIO $ do
  mng <- createManager
  pure Env {
      env'settings = settings
    , env'manager  = mng
    }

instance MonadLauncherConstr t m => MonadLauncher t (ReaderT Env m) where
  getSettings = asks env'settings
  {-# INLINE getSettings #-}
  getManager = asks env'manager
  {-# INLINE getManager #-}

runEnv :: (Reflex t, TriggerEvent t m) => Env -> ReaderT Env (RetractT t m) a -> m a
runEnv e ma = runRetract (runReaderT ma e)

callClient :: MonadLauncher t m
  => Event t (ClientM a) -- ^ Remote action
  -> m (Event t (Either ServantError a))
callClient mae = do
  Settings{..} <- getSettings
  mng <- getManager
  performEventAsync $ ffor mae $ \ae fire ->
    void . liftIO . forkIO $ fire =<< callFutrina settingsAuthServer mng ae

data ClientError = ConnectionError !ServantError | SignatureError !Text
  deriving (Eq, Show, Generic)

printClientError :: ClientError -> Text
printClientError ce = case ce of
  ConnectionError e -> "Failed to connect to server! " <> showt e
  SignatureError e -> "Signature is invalid! " <> e

callSClient :: (MonadLauncher t m, ToJSON a)
  => Event t (ClientM (a, RawSign)) -- ^ Remote action
  -> m (Event t (Either ClientError a))
callSClient mae = do
  Settings{..} <- getSettings
  mng <- getManager
  performEventAsync $ ffor mae $ \ae fire ->
    void . liftIO . forkIO $ do
      res <- callFutrina settingsAuthServer mng ae
      fire $ case res of
        Left es -> Left $ ConnectionError es
        Right as -> case guardAerospaceSign as of
          Left e -> Left $ SignatureError e
          Right a -> Right a

callSClientM :: (MonadLauncher t m, ToJSON a)
  => Event t (ClientM [(a, RawSign)]) -- ^ Remote action
  -> m (Event t (Either ClientError [a]))
callSClientM mae = do
  Settings{..} <- getSettings
  mng <- getManager
  performEventAsync $ ffor mae $ \ae fire ->
    void . liftIO . forkIO $ do
      res <- callFutrina settingsAuthServer mng ae
      fire $ case res of
        Left es -> Left $  ConnectionError es
        Right as -> flip traverse as $ \a -> case guardAerospaceSign a of
          Left e -> Left $ SignatureError e
          Right v -> Right v
