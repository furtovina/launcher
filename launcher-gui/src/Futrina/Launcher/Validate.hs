module Futrina.Launcher.Validate(
    validate
  ) where

import Data.Text (Text)
import Futrina.Launcher.Env
import Reflex.Dom

-- | Helper for widget that displays error
errorWidget :: MonadLauncher t m => Text -> m ()
errorWidget = divClass "validate-error" . text

-- | Print in place error message when value is `Left`. First event tells widget
-- when to clear previous error.
validate :: MonadLauncher t m => Event t () -> Event t (Either Text a) -> m (Event t a)
validate clearE e = do
  let e' = leftmost [Right <$> e, Left <$> clearE]
  widgetHold_ (pure ()) $ ffor e' $ \case
    Right (Left err) -> errorWidget err
    _ -> pure ()
  pure $ fmapMaybe (either (const Nothing) Just) e
