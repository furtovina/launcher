module Futrina.Launcher.Style(
    frontendCss
  , frontendCssBS
  ) where

import Clay
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Futrina.Launcher.Style.TH
import Prelude hiding ((**), rem)

frontendCssBS :: ByteString
frontendCssBS = let
  selfcss = toStrict . encodeUtf8 . renderWith compact [] $ frontendCss
  in milligramCss <> tooltipCss <> selfcss

frontendCss :: Css
frontendCss = do
  html ? textAlign center
  body ? do
    color textColor
    backgroundColor majorBackground
  ".toggled" ? do
    backgroundColor $ rgb 117 48 255
    borderColor $ rgb 117 48 255
  newsCss
  loginFormCss
  checkCss
  downloadCss
  runCss
  errors

textColor :: Color
textColor = rgb 179 184 229

majorBackground :: Color
majorBackground = rgb 24 41 82

minorBackground :: Color
minorBackground = rgb 59 78 122

textAreaBgCol :: Color
textAreaBgCol = rgb 208 216 255

newsCss :: Css
newsCss = do
  ".news" ? do
    width  $ pct 100
    height $ pct 70
    borderRadius (px 10) (px 10) 0 0
    backgroundColor minorBackground
    textAlign $ alignSide sideLeft
    paddingLeft $ px 10
    paddingTop  $ px 15
    overflowY scroll
  ".news-error" ? do
    fontSize $ pt 16
    color $ rgb 240 20 20
  ".spaced-up-text" ? do
    marginTop $ px 20
  ".large-text" ? do
    fontSize $ pt 16
  ".small-text" ? do
    fontSize $ pt 10
  ".red-text" ? do
    color $ rgb 240 20 20
  ".green-text" ? do
    color $ rgb 37 240 31
  ".bold-text" ? do
    fontWeight bold
  ".italic-text" ? do
    fontStyle italic
  ".oblique-text" ? do
    fontStyle oblique
  ".crossed-text" ? do
    textDecoration lineThrough
  ".underlined-text" ? do
    textDecoration underline

loginFormCss :: Css
loginFormCss = do
  form ? do
    marginBottom $ px 0
  fieldset ? do
    marginBottom $ px 0
  ".login-form" ? do
    textAlign $ alignSide sideLeft
    marginTop $ px 15
  ".login-form" ** input ? do
    display inline
    width $ px 150
    marginLeft $ px 15
    color textColor
  ".login-form" ** label ? do
    display inline
    width $ px 150
    marginLeft $ px 15
  ".login-form" ** ".button-primary" ? do
    backgroundColor activeCol
    border solid (rem 0.1) activeCol
    color white
  where
    activeCol = rgb 225 69 148

checkCss :: Css
checkCss = do
  ".check-status" ? do
    marginTop $ px 20
    fontSize $ pt 16

downloadCss :: Css
downloadCss = do
  ".download-status" ? do
    marginTop $ px 20
    fontSize $ pt 16

runCss :: Css
runCss = do
  ".run-status" ? do
    marginTop $ px 20
    fontSize $ pt 16
  ".log-container" ** textarea ? do
    backgroundColor textAreaBgCol
    height $ pct 70

errors :: Css
errors = do
  ".validate-error" ? do
    display inlineBlock
    marginLeft $ px 20
    fontSize $ pt 15
    color $ rgb 255 10 143
