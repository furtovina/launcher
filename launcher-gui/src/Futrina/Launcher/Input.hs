module Futrina.Launcher.Input(
    textField
  , passField
  , submitClass
  ) where

import Data.Text (Text)
import Futrina.Launcher.Elements
import Futrina.Launcher.Env
import Futrina.Launcher.Core.Id
import Reflex.Dom

labeledTextInput :: MonadLauncher t m
  => Text -- ^ Label
  -> TextInputConfig t
  -> m (TextInput t)
labeledTextInput lbl cfg = do
  i <- genId
  label i $ text lbl
  textInput cfg {
      _textInputConfig_attributes = do
        as <- _textInputConfig_attributes cfg
        pure $ "id" =: i <> as
    }

textField :: MonadLauncher t m
  => Text -- ^ Label
  -> Text -- ^ Initial value
  -> m (Dynamic t Text)
textField lbl v0 = fmap _textInput_value $ labeledTextInput lbl def {
    _textInputConfig_initialValue = v0
  }

passField :: MonadLauncher t m
  => Text -- ^ Label
  -> m (Dynamic t Text)
passField lbl = fmap _textInput_value $ labeledTextInput lbl def {
    _textInputConfig_inputType  = "password"
  }

submitClass :: MonadLauncher t m
  => Text -- ^ Class
  -> Text -- ^ Label
  -> m (Event t ())
submitClass cl lbl = do
  (e, _) <- elAttr' "input" (
       "class" =: cl
    <> "type"  =: "submit"
    <> "value" =: lbl) blank
  pure $ domEvent Click e
