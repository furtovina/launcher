# Here you can put overrides of dependencies
{ reflex-platform ? (import ./reflex-platform.nix {}), ... }:
let
  pkgs = reflex-platform.nixpkgs;
  overrideCabal = pkgs.haskell.lib.overrideCabal;
  lib = pkgs.haskell.lib;
  dontHaddock = lib.dontHaddock;
  dontCheck = lib.dontCheck;
  gitignore = pkgs.callPackage (pkgs.fetchFromGitHub {
    owner = "siers";
    repo = "nix-gitignore";
    rev = "ce0778ddd8b1f5f92d26480c21706b51b1af9166";
    sha256 = "1d7ab78i2k13lffskb23x8b5h24x7wkdmpvmria1v3wb9pcpkg2w";
  }) {};
  ingnoreGarbage = pkg: overrideCabal pkg (pkg : let
    ignore-list = ''
      /.stack-work
      /.ghc.environment*
      /dist-newstyle
    '';
    in { src = gitignore.gitignoreSourceAux ignore-list pkg.src; } );

in (self: super: let
  # Internal packages (depends on production or dev environment)
  callInternal = name: path: args: (
    dontHaddock ( self.callCabal2nix name (ingnoreGarbage path) args ));
  in {
    futrina-api = ingnoreGarbage super.futrina-api;
    futrina-auth-server = ingnoreGarbage super.futrina-auth-server;
    futrina-client = ingnoreGarbage super.futrina-client;
    futrina-crypto = ingnoreGarbage super.futrina-crypto;
    futrina-launcher = ingnoreGarbage super.futrina-launcher;
    futrina-launcher-gui = ingnoreGarbage super.futrina-launcher-gui;
    futrina-tools = ingnoreGarbage super.futrina-tools;
    # Overrides from nixpkgs
    servant-client = dontCheck super.servant-client;
    esqueleto = self.callPackage ./derivations/esqueleto.nix {};
    persistent-template = self.callPackage ./derivations/persistent-template.nix {};
    cryptonite = self.callPackage ./derivations/cryptonite.nix {};
    memory = self.callPackage ./derivations/memory.nix {};
    reflex-dom-retractable = self.callPackage ./derivations/reflex-dom-retractable.nix {};
  }
)
